# pubsweet-fira

This is a package of the files in the latest release of [Mozilla's Fira typeface](https://github.com/mozilla/Fira), with changes as described below.

# Additions

1. In fira.css, font-face definitions have been added for "Fira Sans Book". This is actually "Fira Sans" with [font weight 350](https://github.com/carrois/Fira), but it has to be given a different name in the font-face definition as browsers will choose the 300 version when "Fira Sans" with a font-weight of 350 is specified.

# Usage

In an app compiled with Webpack, simply `import 'pubsweet-fira'` then make use of the fonts in CSS as usual.
